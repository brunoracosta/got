import mongoose from 'mongoose';

export interface CharactersDoc {
  name: string;
  tvSeries?: string[];
}

const charactersSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  tvSeries: {
    type: [String],
    required: false,
  },
});

charactersSchema.index({ name: 1 }, { unique: true });

const Character = mongoose.model<CharactersDoc>('Characters', charactersSchema);

export { Character };

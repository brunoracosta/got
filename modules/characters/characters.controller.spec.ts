import { CharactersController } from './characters.controller';
import { mocked } from 'ts-jest/utils';
import { Request, Response } from 'express';
import { CharactersService } from './characters.service';
import { HousesService } from '../houses/houses.service';
import { CharactersDoc } from './schema/characters.schema';

jest.mock('./characters.service');
jest.mock('../houses/houses.service');

describe('CharactersController', () => {
  let controller: CharactersController;

  beforeEach(() => {
    controller = new CharactersController();
    mocked(CharactersService).mockClear();
    mocked(HousesService).mockClear();
  });

  describe('create', () => {
    it('With invalid Body', async (): Promise<void> => {
      const expectedAnswer: string[] = ['name is a required field'];

      const res = {
        status: (paramStatus: any): Response => {
          expect(paramStatus).toEqual(400);
          return res;
        },
        send: (paramResult: string[]) => {
          expect(paramResult).toEqual(expectedAnswer);
        },
      } as Response;

      const req: any = {
        query: {},
        params: {},
        headers: {},
        body: {},
      } as Request;

      await controller.create(req, res);
    });

    it('With nameDuplicated', async (): Promise<void> => {
      const expectedAnswer: string[] = ['Name duplicated'];

      const res = {
        status: (paramStatus: any): Response => {
          expect(paramStatus).toEqual(400);
          return res;
        },
        send: (paramResult: string[]) => {
          expect(paramResult).toEqual(expectedAnswer);
        },
      } as Response;

      const req: any = {
        query: {},
        params: {},
        headers: {},
        body: {
          name: 'name',
        },
      } as Request;

      CharactersService.prototype.create = jest
        .fn()
        .mockImplementation((param) => {
          throw new Error('duplicate key');
        });

      await controller.create(req, res);
    });

    it('With others erros', async (): Promise<void> => {
      const expectedAnswer = 'Erro não identificado.';

      const res = {
        status: (paramStatus: any): Response => {
          expect(paramStatus).toEqual(500);
          return res;
        },
        send: (paramResult: string[]) => {
          expect(paramResult).toEqual(expectedAnswer);
        },
      } as Response;

      const req: any = {
        query: {},
        params: {},
        headers: {},
        body: {
          name: 'name',
        },
      } as Request;

      CharactersService.prototype.create = jest
        .fn()
        .mockImplementation((param) => {
          throw new Error();
        });

      await controller.create(req, res);
    });

    it('Works', async (): Promise<void> => {
      const expectedAnswer: CharactersDoc = {
        name: 'CharactersDoc',
      };

      const res = {
        status: (paramStatus: any): Response => {
          expect(paramStatus).toEqual(201);
          return res;
        },
        send: (paramResult: string[]) => {
          expect(paramResult).toEqual(expectedAnswer);
        },
      } as Response;

      const req: any = {
        query: {},
        params: {},
        headers: {},
        body: {
          name: 'name',
        },
      } as Request;

      CharactersService.prototype.create = jest
        .fn()
        .mockResolvedValue(expectedAnswer);

      await controller.create(req, res);
    });
  });

  describe('update', () => {
    it('With invalid Body', async (): Promise<void> => {
      const expectedAnswer: string[] = ['name is a required field'];

      const res = {
        status: (paramStatus: any): Response => {
          expect(paramStatus).toEqual(400);
          return res;
        },
        send: (paramResult: string[]) => {
          expect(paramResult).toEqual(expectedAnswer);
        },
      } as Response;

      const req: any = {
        query: {},
        params: {},
        headers: {},
        body: {},
      } as Request;

      await controller.update(req, res);
    });

    it('With nameDuplicated', async (): Promise<void> => {
      const expectedAnswer: string[] = ['Name duplicated'];

      const res = {
        status: (paramStatus: any): Response => {
          expect(paramStatus).toEqual(400);
          return res;
        },
        send: (paramResult: string[]) => {
          expect(paramResult).toEqual(expectedAnswer);
        },
      } as Response;

      const req: any = {
        query: {},
        params: {},
        headers: {},
        body: {
          name: 'name',
        },
      } as Request;

      CharactersService.prototype.update = jest
        .fn()
        .mockImplementation((param) => {
          throw new Error('duplicate key');
        });

      await controller.update(req, res);
    });

    it('With others erros', async (): Promise<void> => {
      const expectedAnswer = 'Erro não identificado.';

      const res = {
        status: (paramStatus: any): Response => {
          expect(paramStatus).toEqual(500);
          return res;
        },
        send: (paramResult: string[]) => {
          expect(paramResult).toEqual(expectedAnswer);
        },
      } as Response;

      const req: any = {
        query: {},
        params: {},
        headers: {},
        body: {
          name: 'name',
        },
      } as Request;

      CharactersService.prototype.update = jest
        .fn()
        .mockImplementation((param) => {
          throw new Error();
        });

      await controller.update(req, res);
    });

    it('Character not found', async (): Promise<void> => {
      const expectedAnswer = ['Character not found'];

      const res = {
        status: (paramStatus: any): Response => {
          expect(paramStatus).toEqual(400);
          return res;
        },
        send: (paramResult: string[]) => {
          expect(paramResult).toEqual(expectedAnswer);
        },
      } as Response;

      const req: any = {
        query: {},
        params: {},
        headers: {},
        body: {
          name: 'name',
        },
      } as Request;

      CharactersService.prototype.update = jest
        .fn()
        .mockImplementation((param) => {
          return null;
        });

      await controller.update(req, res);
    });

    it('works', async (): Promise<void> => {
      const expectedAnswer: CharactersDoc = {
        name: 'CharactersDoc',
      };

      const res = {
        status: (paramStatus: any): Response => {
          expect(paramStatus).toEqual(200);
          return res;
        },
        send: (paramResult: string[]) => {
          expect(paramResult).toEqual(expectedAnswer);
        },
      } as Response;

      const req: any = {
        query: {},
        params: {},
        headers: {},
        body: {
          name: 'name',
        },
      } as Request;

      HousesService.prototype.updateLord = jest
        .fn()
        .mockResolvedValue(expectedAnswer);

      CharactersService.prototype.update = jest
        .fn()
        .mockResolvedValue(expectedAnswer);

      await controller.update(req, res);
    });
  });
});

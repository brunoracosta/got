import mongoose from 'mongoose';
import { Character, CharactersDoc } from './schema/characters.schema';

export class CharactersService {
  public async create(paramCharacters: CharactersDoc): Promise<CharactersDoc> {
    return Character.create(paramCharacters);
  }

  public async get(paramId: string): Promise<CharactersDoc | null> {
    if (mongoose.isValidObjectId(paramId)) {
      return Character.findById(paramId);
    }
    return null;
  }

  public async update(
    paramId: string,
    paramCharacters: CharactersDoc
  ): Promise<CharactersDoc | null> {
    if (mongoose.isValidObjectId(paramId)) {
      return Character.findOneAndUpdate(
        { _id: mongoose.Types.ObjectId(paramId) },
        {
          $set: paramCharacters,
        },
        { new: true }
      );
    }
    return null;
  }
}

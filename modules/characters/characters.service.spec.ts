import { mocked } from 'ts-jest/utils';
import { CharactersService } from './characters.service';
import { Character, CharactersDoc } from './schema/characters.schema';
import mongoose from 'mongoose';

jest.mock('./schema/characters.schema');

describe('CharactersService', () => {
  let controller: CharactersService;

  beforeEach(() => {
    controller = new CharactersService();
    mocked(Character).mockClear();
  });

  describe('create', () => {
    it('Works', async (): Promise<void> => {
      const expectedAnswer: CharactersDoc = {
        name: 'name',
      };

      Character.create = jest.fn().mockResolvedValue(expectedAnswer);

      const resp = await controller.create(expectedAnswer);
      expect(resp).toEqual(expectedAnswer);
    });
  });

  describe('get', () => {
    it('With invalid Id', async (): Promise<void> => {
      const id: string = 'id';

      const resp = await controller.get(id);
      expect(resp).toBeNull();
    });

    it('With valid Id', async (): Promise<void> => {
      const expectedAnswer: CharactersDoc = {
        name: 'name',
      };
      const id = mongoose.Types.ObjectId().toString();

      Character.findById = jest.fn().mockResolvedValue(expectedAnswer);

      const resp = await controller.get(id);
      expect(resp).toBe(expectedAnswer);
    });
  });

  describe('update', () => {
    it('With invalid Id', async (): Promise<void> => {
      const expectedAnswer: CharactersDoc = {
        name: 'name',
      };
      const id: string = 'id';

      const resp = await controller.update(id, expectedAnswer);
      expect(resp).toBeNull();
    });

    it('With valid Id', async (): Promise<void> => {
      const expectedAnswer: CharactersDoc = {
        name: 'name',
      };
      const id = mongoose.Types.ObjectId().toString();

      Character.findOneAndUpdate = jest.fn().mockResolvedValue(expectedAnswer);

      const resp = await controller.update(id, expectedAnswer);
      expect(resp).toBe(expectedAnswer);
    });
  });
});

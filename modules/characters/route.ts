import { Router } from 'express';
import { CharactersController } from './characters.controller';

const routes = Router();

const nameModule = '/characters';
const constroller = new CharactersController();

routes.post(nameModule, constroller.create);

routes.put(`${nameModule}/:id`, constroller.update);

export default routes;

import { Request, Response } from 'express';
import { CharactersService } from './characters.service';
import { SchemaOf, string, object, ValidationError, array } from 'yup';
import { CharactersDoc } from './schema/characters.schema';
import { HousesService } from '../houses/houses.service';

const houseType: SchemaOf<CharactersDoc> = object({
  name: string().min(3).required(),
  tvSeries: array(),
}).defined();

export class CharactersController {
  public async create(req: Request, res: Response): Promise<void> {
    try {
      const validateBody = await houseType.validate(req.body, {
        abortEarly: false,
      });

      const charactersService = new CharactersService();
      const character = await charactersService.create(validateBody);
      res.status(201).send(character);
    } catch (error) {
      if (error instanceof ValidationError) {
        res.status(400).send(error.errors);
      } else if (error.message.indexOf('duplicate key') !== -1) {
        res.status(400).send(['Name duplicated']);
      } else {
        res.status(500).send('Erro não identificado.');
      }
    }
  }

  public async update(req: Request, res: Response): Promise<void> {
    try {
      const validateBody = await houseType.validate(req.body, {
        abortEarly: false,
      });

      const charactersService = new CharactersService();
      const character = await charactersService.update(
        req.params.id,
        validateBody
      );

      if (!character) {
        res.status(400).send(['Character not found']);
        return;
      }

      const housesService = new HousesService();
      await housesService.updateLord(req.params.id, validateBody.name);

      res.status(200).send(character);
    } catch (error) {
      if (error instanceof ValidationError) {
        res.status(400).send(error.errors);
      } else if (error.message.indexOf('duplicate key') !== -1) {
        res.status(400).send(['Name duplicated']);
      } else {
        res.status(500).send('Erro não identificado.');
      }
    }
  }
}

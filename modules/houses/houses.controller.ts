import { Request, Response } from 'express';
import { HousesService } from './houses.service';
import { SchemaOf, string, object, ValidationError } from 'yup';
import { HousesDoc } from './schema/houses.schema';
import { CharactersService } from '../characters/characters.service';

const houseType: SchemaOf<HousesDoc> = object({
  name: string().min(3).required(),
  region: string().min(3).required(),
  currentLordId: string(),
  currentLord: string().default(''),
  founded: string(),
}).defined();

export class HousesController {
  public async list(req: Request, res: Response): Promise<void> {
    let name;
    if (req.query.search) {
      name = req.query.search as string;
    }
    const housesService = new HousesService();
    const list = await housesService.list(name);
    res.send(list);
  }

  public async get(req: Request, res: Response): Promise<void> {
    try {
      const housesService = new HousesService();

      const house = await housesService.get(req.params.id);

      let status = 204;
      if (house) {
        status = 200;
      }
      res.status(status).send(house);
    } catch (error) {
      res.status(500).send('Erro não identificado.');
    }
  }

  public async create(req: Request, res: Response): Promise<void> {
    try {
      const validateBody = await houseType.validate(req.body, {
        abortEarly: false,
      });

      const charactersService = new CharactersService();

      validateBody.currentLord = '';
      if (validateBody.currentLordId) {
        const lord = await charactersService.get(validateBody.currentLordId);

        if (!lord) {
          res.status(400).send(['Lord not found.']);
          return;
        }

        validateBody.currentLord = lord.name;
      }

      const housesService = new HousesService();

      const list = await housesService.create(validateBody);
      res.status(201).send(list);
    } catch (error) {
      if (error instanceof ValidationError) {
        res.status(400).send(error.errors);
      } else {
        res.status(500).send('Erro não identificado.');
      }
    }
  }

  public async delete(req: Request, res: Response): Promise<void> {
    try {
      const housesService = new HousesService();

      const house = await housesService.delete(req.params.id);

      if (house) {
        res.status(200).send();
        return;
      }
      res.status(400).send('House not found.');
    } catch (error) {
      res.status(500).send('Erro não identificado.');
    }
  }
}

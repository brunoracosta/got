import mongoose from 'mongoose';

export interface HousesDoc {
  name: string;
  region: string;
  currentLordId?: string;
  currentLord?: string;
  founded?: string;
}

const housesSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  region: {
    type: String,
    required: true,
  },
  currentLordId: {
    type: mongoose.Types.ObjectId,
    required: false,
  },
  currentLord: {
    type: String,
    required: false,
    default: '',
  },
  founded: {
    type: String,
    required: false,
    default: '',
  },
});

housesSchema.index({ name: 1, region: 1 }, { unique: true });
housesSchema.index({ currentLordId: 1 });

const House = mongoose.model<HousesDoc>('Houses', housesSchema);

export { House };

import { mocked } from 'ts-jest/utils';
import { HousesService } from './houses.service';
import { House, HousesDoc } from './schema/houses.schema';
import mongoose from 'mongoose';

jest.mock('./schema/houses.schema');

describe('HousesService', () => {
  let controller: HousesService;

  beforeEach(() => {
    controller = new HousesService();
    mocked(House).mockClear();
  });

  describe('list', () => {
    it('Without search', async (): Promise<void> => {
      const expectedAnswer: HousesDoc[] = [];

      House.find = jest
        .fn()
        .mockImplementation((param): Promise<HousesDoc[]> => {
          expect(param.name).toBeUndefined();
          return Promise.resolve(expectedAnswer);
        });

      const resp = await controller.list();
      expect(resp).toEqual(expectedAnswer);
    });

    it('With search', async (): Promise<void> => {
      const expectedAnswer: HousesDoc[] = [];
      const search = 'teste';

      House.find = jest
        .fn()
        .mockImplementation((param): Promise<HousesDoc[]> => {
          expect(param.name).toBeDefined();
          return Promise.resolve(expectedAnswer);
        });

      const resp = await controller.list(search);
      expect(resp).toEqual(expectedAnswer);
    });
  });

  describe('create', () => {
    it('Works', async (): Promise<void> => {
      const expectedAnswer: HousesDoc = {
        name: 'name',
        region: 'reg',
      };

      House.create = jest.fn().mockResolvedValue(expectedAnswer);

      const resp = await controller.create(expectedAnswer);
      expect(resp).toEqual(expectedAnswer);
    });
  });

  describe('get', () => {
    it('With invalid Id', async (): Promise<void> => {
      const id: string = 'id';

      const resp = await controller.get(id);
      expect(resp).toBeNull();
    });

    it('With valid Id', async (): Promise<void> => {
      const expectedAnswer: HousesDoc = {
        name: 'name',
        region: 'reg',
      };
      const id = mongoose.Types.ObjectId().toString();

      House.findById = jest.fn().mockResolvedValue(expectedAnswer);

      const resp = await controller.get(id);
      expect(resp).toBe(expectedAnswer);
    });
  });

  describe('delete', () => {
    it('With invalid Id', async (): Promise<void> => {
      const id: string = 'id';

      const resp = await controller.delete(id);
      expect(resp).toBeNull();
    });

    it('With valid Id', async (): Promise<void> => {
      const expectedAnswer: HousesDoc = {
        name: 'name',
        region: 'reg',
      };
      const id = mongoose.Types.ObjectId().toString();

      House.deleteOne = jest.fn().mockResolvedValue(expectedAnswer);

      const resp = await controller.delete(id);
      expect(resp).toBe(expectedAnswer);
    });
  });

  describe('updateLord', () => {
    it('Works', async (): Promise<void> => {
      House.updateMany = jest.fn().mockResolvedValue('');

      const resp = await controller.updateLord('id', 'name');
      expect(resp).toBeUndefined();
    });
  });
});

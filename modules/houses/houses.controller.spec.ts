import { HousesController } from './houses.controller';
import { mocked } from 'ts-jest/utils';
import { HousesService } from './houses.service';
import { Request, Response } from 'express';
import { HousesDoc } from './schema/houses.schema';
import { CharactersService } from '../characters/characters.service';

jest.mock('./houses.service');
jest.mock('../characters/characters.service');
jest.mock('express');

describe('HousesController', () => {
  let controller: HousesController;

  beforeEach(() => {
    controller = new HousesController();
    mocked(HousesService).mockClear();
    mocked(CharactersService).mockClear();
  });

  describe('list', () => {
    it('Lista sem busca', async (): Promise<void> => {
      const expectedAnswer: HousesDoc[] = [
        {
          name: 'name',
          region: 'region',
        },
      ];

      const res = {
        send: (paramResult: HousesDoc[]) => {
          expect(paramResult).toEqual(expectedAnswer);
        },
      } as Response;

      const req: any = {
        query: {},
        params: {},
        headers: {},
      } as Request;

      HousesService.prototype.list = jest.fn().mockImplementation((param) => {
        expect(param).toBeUndefined();
        return expectedAnswer;
      });

      await controller.list(req, res);
    });

    it('Lista com busca', async (): Promise<void> => {
      const expectedAnswer: HousesDoc[] = [
        {
          name: 'nameCom',
          region: 'regionCom',
        },
      ];

      const res = {
        send: (paramResult: HousesDoc[]) => {
          expect(paramResult).toEqual(expectedAnswer);
        },
      } as Response;

      const req: any = {
        query: {},
        params: {},
        headers: {},
      } as Request;

      req.query['search'] = 'search';

      HousesService.prototype.list = jest
        .fn()
        .mockImplementation((param: string) => {
          expect(param).toEqual(req.query['search']);
          return expectedAnswer;
        });

      await controller.list(req, res);
    });
  });

  describe('get', () => {
    it('With invalid ID', async (): Promise<void> => {
      const expectedAnswer = undefined;

      const res = {
        status: (paramStatus: any): Response => {
          expect(paramStatus).toEqual(204);
          return res;
        },
        send: (paramResult: HousesDoc[]) => {
          expect(paramResult).toEqual(expectedAnswer);
        },
      } as Response;

      const req: any = {
        query: {},
        params: {},
        headers: {},
      } as Request;

      HousesService.prototype.get = jest.fn().mockImplementation((param) => {
        expect(param).toBeUndefined();
        return expectedAnswer;
      });

      await controller.get(req, res);
    });

    it('With valid ID', async (): Promise<void> => {
      const expectedAnswer: HousesDoc = {
        name: 'nameCom',
        region: 'regionCom',
      };

      const res = {
        status: (paramStatus: any): Response => {
          expect(paramStatus).toEqual(200);
          return res;
        },
        send: (paramResult: HousesDoc[]) => {
          expect(paramResult).toEqual(expectedAnswer);
        },
      } as Response;

      const req: any = {
        query: {},
        params: {},
        headers: {},
      } as Request;

      HousesService.prototype.get = jest.fn().mockImplementation((param) => {
        expect(param).toBeUndefined();
        return expectedAnswer;
      });

      await controller.get(req, res);
    });

    it('With Error', async (): Promise<void> => {
      const expectedAnswer = 'Erro não identificado.';

      const res = {
        status: (paramStatus: any): Response => {
          expect(paramStatus).toEqual(500);
          return res;
        },
        send: (paramResult: HousesDoc[]) => {
          expect(paramResult).toEqual(expectedAnswer);
        },
      } as Response;

      const req: any = {
        query: {},
        params: {},
        headers: {},
      } as Request;

      HousesService.prototype.get = jest.fn().mockImplementation((param) => {
        expect(param).toBeUndefined();
        throw new Error();
      });

      await controller.get(req, res);
    });
  });

  describe('delete', () => {
    it('With invalid ID', async (): Promise<void> => {
      const expectedAnswer = 'House not found.';

      const res = {
        status: (paramStatus: any): Response => {
          expect(paramStatus).toEqual(400);
          return res;
        },
        send: (paramResult: HousesDoc[]) => {
          expect(paramResult).toEqual(expectedAnswer);
        },
      } as Response;

      const req: any = {
        query: {},
        params: {},
        headers: {},
      } as Request;

      HousesService.prototype.delete = jest.fn().mockImplementation((param) => {
        expect(param).toBeUndefined();
        return null;
      });

      await controller.delete(req, res);
    });

    it('With valid ID', async (): Promise<void> => {
      const expectedAnswer = 'Delete';

      const res = {
        status: (paramStatus: any): Response => {
          expect(paramStatus).toEqual(200);
          return res;
        },
        send: (paramResult: HousesDoc[]) => {
          expect(paramResult).toBeUndefined();
        },
      } as Response;

      const req: any = {
        query: {},
        params: {},
        headers: {},
      } as Request;

      HousesService.prototype.delete = jest.fn().mockImplementation((param) => {
        expect(param).toBeUndefined();
        return expectedAnswer;
      });

      await controller.delete(req, res);
    });

    it('With Error', async (): Promise<void> => {
      const expectedAnswer = 'Erro não identificado.';

      const res = {
        status: (paramStatus: any): Response => {
          expect(paramStatus).toEqual(500);
          return res;
        },
        send: (paramResult: HousesDoc[]) => {
          expect(paramResult).toEqual(expectedAnswer);
        },
      } as Response;

      const req: any = {
        query: {},
        params: {},
        headers: {},
      } as Request;

      HousesService.prototype.delete = jest.fn().mockImplementation((param) => {
        expect(param).toBeUndefined();
        throw new Error();
      });

      await controller.delete(req, res);
    });
  });

  describe('create', () => {
    it('With ivalid Body', async (): Promise<void> => {
      const expectedAnswer: string[] = [
        'name is a required field',
        'region is a required field',
      ];

      const res = {
        status: (paramStatus: any): Response => {
          expect(paramStatus).toEqual(400);
          return res;
        },
        send: (paramResult: HousesDoc[]) => {
          expect(paramResult).toEqual(expectedAnswer);
        },
      } as Response;

      const req: any = {
        query: {},
        params: {},
        headers: {},
        body: {},
      } as Request;

      await controller.create(req, res);
    });

    it('With name and region less than 3', async (): Promise<void> => {
      const expectedAnswer: string[] = [
        'name must be at least 3 characters',
        'region must be at least 3 characters',
      ];

      const res = {
        status: (paramStatus: any): Response => {
          expect(paramStatus).toEqual(400);
          return res;
        },
        send: (paramResult: HousesDoc[]) => {
          expect(paramResult).toEqual(expectedAnswer);
        },
      } as Response;

      const req: any = {
        query: {},
        params: {},
        headers: {},
        body: {
          name: 'na',
          region: 're',
        },
      } as Request;

      await controller.create(req, res);
    });

    it('Without currentLordId', async (): Promise<void> => {
      const expectedAnswer: HousesDoc = {
        name: 'name',
        region: 'region',
        currentLord: '',
      };

      const res = {
        status: (paramStatus: any): Response => {
          expect(paramStatus).toEqual(201);
          return res;
        },
        send: (paramResult: HousesDoc) => {
          expect(paramResult).toEqual(expectedAnswer);
        },
      } as Response;

      const req: any = {
        query: {},
        params: {},
        headers: {},
        body: {
          name: 'name',
          region: 'region',
          currentLord: 'currentLord',
        },
      } as Request;

      HousesService.prototype.create = jest
        .fn()
        .mockImplementation((param: HousesDoc) => {
          expect(param).toEqual(expectedAnswer);
          return expectedAnswer;
        });

      await controller.create(req, res);
    });

    it('Without currentLordId and throw Error', async (): Promise<void> => {
      const expectedAnswer: HousesDoc = {
        name: 'name',
        region: 'region',
        currentLord: '',
      };

      const res = {
        status: (paramStatus: any): Response => {
          expect(paramStatus).toEqual(500);
          return res;
        },
        send: (paramResult: HousesDoc) => {
          expect(paramResult).toEqual('Erro não identificado.');
        },
      } as Response;

      const req: any = {
        query: {},
        params: {},
        headers: {},
        body: {
          name: 'name',
          region: 'region',
          currentLord: 'currentLord',
        },
      } as Request;

      HousesService.prototype.create = jest
        .fn()
        .mockImplementation((param: HousesDoc) => {
          expect(param).toEqual(expectedAnswer);
          throw new Error();
        });

      await controller.create(req, res);
    });

    it('With currentLordId but not found', async (): Promise<void> => {
      const expectedAnswer: string[] = ['Lord not found.'];

      const res = {
        status: (paramStatus: any): Response => {
          expect(paramStatus).toEqual(400);
          return res;
        },
        send: (paramResult: HousesDoc) => {
          expect(paramResult).toEqual(expectedAnswer);
        },
      } as Response;

      const req: any = {
        query: {},
        params: {},
        headers: {},
        body: {
          name: 'name',
          region: 'region',
          currentLord: 'currentLord',
          currentLordId: 'currentLordId',
        },
      } as Request;

      CharactersService.prototype.get = jest
        .fn()
        .mockImplementation((param: HousesDoc) => {
          expect(param).toEqual(req.body.currentLordId);
          return null;
        });

      HousesService.prototype.create = jest
        .fn()
        .mockImplementation((param: HousesDoc) => {
          throw new Error('Should not call.');
        });

      await controller.create(req, res);
    });

    it('Without currentLordId', async (): Promise<void> => {
      const expectedAnswer: HousesDoc = {
        name: 'name',
        region: 'region',
        currentLord: 'NEwNAME',
        currentLordId: 'currentLordId',
      };

      const res = {
        status: (paramStatus: any): Response => {
          expect(paramStatus).toEqual(201);
          return res;
        },
        send: (paramResult: HousesDoc) => {
          expect(paramResult).toEqual(expectedAnswer);
        },
      } as Response;

      const req: any = {
        query: {},
        params: {},
        headers: {},
        body: {
          name: 'name',
          region: 'region',
          currentLord: 'currentLord',
          currentLordId: 'currentLordId',
        },
      } as Request;

      CharactersService.prototype.get = jest
        .fn()
        .mockImplementation((param: HousesDoc) => {
          expect(param).toEqual(req.body.currentLordId);
          return {
            name: expectedAnswer.currentLord,
          };
        });

      HousesService.prototype.create = jest
        .fn()
        .mockImplementation((param: HousesDoc) => {
          expect(param).toEqual(expectedAnswer);
          return expectedAnswer;
        });

      await controller.create(req, res);
    });
  });
});

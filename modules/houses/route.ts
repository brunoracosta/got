import { Router } from 'express';
import { HousesController } from './houses.controller';

const routes = Router();

const nameModule = '/houses';
const constroller = new HousesController();

routes.get(nameModule, constroller.list);

routes.get(`${nameModule}/:id`, constroller.get);

routes.post(nameModule, constroller.create);

routes.delete(`${nameModule}/:id`, constroller.delete);

export default routes;

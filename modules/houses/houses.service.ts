import { House, HousesDoc } from './schema/houses.schema';
import mongoose from 'mongoose';

export class HousesService {
  public async list(paramSearch?: string): Promise<HousesDoc[]> {
    let filter: any = {};
    if (paramSearch) {
      filter['name'] = new RegExp(paramSearch.trim(), 'i');
    }
    return House.find(filter);
  }

  public async create(paramHouse: HousesDoc): Promise<HousesDoc> {
    return House.create(paramHouse);
  }

  public async get(paramId: string): Promise<HousesDoc | null> {
    if (mongoose.isValidObjectId(paramId)) {
      return House.findById(paramId);
    }
    return null;
  }

  public async delete(paramId: string): Promise<any> {
    if (mongoose.isValidObjectId(paramId)) {
      return House.deleteOne({ _id: mongoose.Types.ObjectId(paramId) });
    }
    return null;
  }

  public async updateLord(paramId: string, paramName: string): Promise<void> {
    await House.updateMany(
      {
        currentLordId: paramId,
      },
      {
        $set: {
          currentLord: paramName,
        },
      }
    );
    return;
  }
}

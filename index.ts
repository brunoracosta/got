import express from 'express';
import { json } from 'body-parser';
import { sync } from 'glob';
import path from 'path';
import helmet from 'helmet';
import { connectDb } from './util/database.connection';

// rest of the code remains same

const app = express();
app.use(json());
app.use(helmet());

const PORT = 8000;

connectDb();

app.get('/', (req, res) => res.send('Express + TypeScript Server'));

sync(path.join(path.dirname(__filename), 'modules/**/route.*')).forEach(
  (file) => {
    const moduleName = path.basename(path.dirname(file));
    console.log(`Importing [${moduleName}] routes from [${file}]`);
    const routes = require(path.resolve(file));
    app.use('/', routes.default);
  }
);

app.listen(PORT, () => {
  console.log(`⚡️[server]: Server is running at https://localhost:${PORT}`);
});

import { connectDb } from '../util/database.connection';
import mongoose from 'mongoose';
import { Character } from '../modules/characters/schema/characters.schema';
import { House } from '../modules/houses/schema/houses.schema';

const createDefaultData = async () => {
  const db = await connectDb();

  await House.deleteMany();
  await Character.deleteMany();

  const delonne = {
    _id: mongoose.Types.ObjectId(),
    url: 'https://anapioficeandfire.com/api/characters/298',
    name: 'Delonne Allyrion',
    tvSeries: [''],
  };

  const arthur = {
    _id: mongoose.Types.ObjectId(),
    url: 'https://anapioficeandfire.com/api/characters/141',
    name: 'Arthur Ambrose',
    tvSeries: [''],
  };

  const robert = {
    _id: mongoose.Types.ObjectId(),
    url: 'https://www.anapioficeandfire.com/api/characters/894',
    name: 'Robert Arryn',
    tvSeries: ['Season 1', 'Season 4', 'Season 5'],
  };

  const petyr = {
    _id: mongoose.Types.ObjectId(),
    url: 'https://anapioficeandfire.com/api/characters/823',
    name: 'Petyr Baelish',
    tvSeries: [
      'Season 1',
      'Season 2',
      'Season 3',
      'Season 4',
      'Season 5',
      'Season 6',
    ],
  };

  const houses = [
    {
      name: 'House Algood',
      region: 'The Westerlands',
      currentLord: '',
      founded: '',
    },
    {
      name: 'House Allyrion of Godsgrace',
      region: 'Dorne',
      currentLordId: delonne._id,
      currentLord: delonne.name,
      founded: '',
    },
    {
      name: 'House Amber',
      region: 'The North',
      currentLord: '',
      founded: '',
    },
    {
      name: 'House Ambrose',
      region: 'The Reach',
      currentLordId: arthur._id,
      currentLord: arthur.name,
      founded: '',
    },
    {
      name: 'House Appleton of Appleton',
      region: 'The Reach',
      currentLord: '',
      founded: '',
    },
    {
      name: 'House Arryn of Gulltown',
      region: 'The Vale',
      currentLord: '',
      founded: '',
    },
    {
      name: 'House Arryn of the Eyrie',
      region: 'The Vale',
      currentLordId: robert._id,
      currentLord: robert.name,
      founded: 'Coming of the Andals',
    },
    {
      name: 'House Ashford of Ashford',
      region: 'The Reach',
      currentLord: '',
      founded: '',
    },
    {
      name: 'House Ashwood',
      region: 'The North',
      currentLord: '',
      founded: '',
    },
    {
      name: 'House Baelish of Harrenhal',
      region: 'The Riverlands',
      currentLordId: petyr._id,
      currentLord: petyr.name,
      founded: '299 AC',
    },
  ];

  await Character.insertMany([delonne, arthur, robert, petyr]);
  await House.insertMany(houses);

  mongoose.connection.close();
};

createDefaultData();

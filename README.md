## Getting Started

**Versão do Node**

Node v14.17.5

---

**Instalando dependências**

Caso não tenha instalado **nodemon**, **typescript** e **jest**, realize a intalação desses pacotes:

```powershell
npm install -g nodemon jest typescript
```

**Executando a aplicação**

O projeto utiliza o **mongoDb**, para add a banco de testes execute:

```powershell
npm run seed
```

Para executar

```powershell
npm start
```

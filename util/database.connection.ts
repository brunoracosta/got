import mongoose from 'mongoose';
import config from './config/default.json';

export const connectDb = async (): Promise<void> => {
  mongoose.set('useFindAndModify', false);
  await mongoose.connect(config.db.url, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
  });
  console.log('connected to database');
};
